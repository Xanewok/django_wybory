from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from wybory.models import Kandydat, Wojewodztwo, Okreg, Powiat, Gmina, Obwod, Wyniki
from wybory.views import try_update_vote_count

# Register your models here.


@admin.register(Kandydat)
class KandydatAdmin(admin.ModelAdmin):
    list_display = ('nazwisko', 'imie',)
    search_fields = ('nazwisko', 'imie',)


@admin.register(Wojewodztwo)
class WojewodztwoAdmin(admin.ModelAdmin):
    list_display = ('nazwa',)
    search_fields = ('nazwa',)


@admin.register(Okreg)
class OkregAdmin(admin.ModelAdmin):
    list_display = ('nazwa',)
    search_fields = ('id',)

    def nazwa(self, obj):
        return str(obj)

    nazwa.admin_order_field = 'id'


@admin.register(Powiat)
class PowiatAdmin(admin.ModelAdmin):
    list_display = ('nazwa', 'wojewodztwo')
    search_fields = ('nazwa', 'wojewodztwo__nazwa')
    raw_id_fields = ('wojewodztwo',)


@admin.register(Gmina)
class GminaAdmin(admin.ModelAdmin):
    list_display = ('nazwa', 'powiat', 'kod')
    search_fields = ('nazwa', 'powiat__nazwa', 'kod')
    raw_id_fields = ('powiat',)


# https://github.com/django/django/blob/master/django/contrib/admin/options.py#L1403
# Validation in changeview in ModelAdmin is wrapped in transaction.atomic() so partial
# validation can't leave data in a corrupted/inconsistent state

class WynikInlineFormSet(BaseInlineFormSet):
    def clean(self):
        super(WynikInlineFormSet, self).clean()
        total = 0
        for form in self.forms:
            if not form.is_valid():
                return  # other errors exist, so don't bother
            if form.cleaned_data:
                total += form.cleaned_data['glosy']
        if total > self.instance.wydane_karty:
            raise ValidationError("Suma głosów z wnyików jest większa niż liczba głosów ważnych w obwodzie")

    def save_existing_objects(self, commit=True):
        if commit:
            for form in filter(lambda x: x.has_changed(), self.forms):
                wyniki = form.cleaned_data['id']
                new_vote = form.cleaned_data['glosy']
                # Updates Obwod object for every related, changed Wyniki object
                try_update_vote_count(self.instance, wyniki.kandydat, new_vote)

        saved_instances = super(WynikInlineFormSet, self).save_existing_objects(commit)

        return saved_instances


class ObwodWynikiInline(admin.TabularInline):
    model = Wyniki
    formset = WynikInlineFormSet
    readonly_fields = ('kandydat',)
    can_delete = False
    extra = 0


@admin.register(Obwod)
class ObwodAdmin(admin.ModelAdmin):
    list_display = ('nazwa', 'okreg', 'gmina')
    search_fields = ('adres', 'okreg__id', 'gmina__nazwa')
    raw_id_fields = ('okreg', 'gmina')
    readonly_fields = ('glosy_oddane', 'glosy_niewazne', 'glosy_wazne',)
    exclude = ('wyniki_kandydatow',)  # We edit those via ObwodWynikiInline
    inlines = [
        ObwodWynikiInline,
    ]

    def nazwa(self, obj):
        return obj.adres
