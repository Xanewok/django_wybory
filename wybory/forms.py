from django import forms
from wybory.models import Kandydat, Gmina, validate_non_negative


class GminaSearchForm(forms.ModelForm):
    class Meta:
        model = Gmina
        fields = {'nazwa'}


class WynikiForm(forms.Form):
    kandydat = forms.ModelChoiceField(Kandydat.objects)
    glosy = forms.IntegerField(label='Głosy', validators=[validate_non_negative])

    def clean(self):
        if not Kandydat.objects.filter(pk=self.cleaned_data['kandydat'].id):
            self.add_error("kandydat", "Kandydat nie istnieje.")
