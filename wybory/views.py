from collections import OrderedDict
from django.shortcuts import render
from django.contrib.auth import authenticate
from django.http import JsonResponse, HttpResponseNotFound, Http404
from django.template import loader
from django.views import View
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from django.db import transaction, IntegrityError
from django.db.models import Sum, FloatField
from rest_framework import status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework import serializers
from rest_framework.authtoken.models import Token


from wybory.models import Kandydat, Wojewodztwo, Okreg, Gmina, Obwod, Wyniki
from wybory.models import build_navigation
from wybory.forms import GminaSearchForm, WynikiForm

from django.shortcuts import get_object_or_404


# Create your views here.
def kraj_view(request, **kwargs):
    kwargs['wyniki'] = Kandydat.objects.all()

    return single_results_view(request, template_name="kraj.html", **kwargs)


def wojewodztwo_view(request, **kwargs):
    kwargs['obj'] = Wojewodztwo.objects.get(pk=kwargs['id'])
    kwargs['wyniki'] = Kandydat.objects.filter(wyniki__obwod__okreg__wojewodztwo_id=kwargs['id'])

    return single_results_view(request, **kwargs)


def okreg_view(request, **kwargs):
    kwargs['obj'] = Okreg.objects.get(pk=kwargs['id'])
    kwargs['wyniki'] = Kandydat.objects.filter(wyniki__obwod__okreg_id=kwargs['id'])

    return single_results_view(request, **kwargs)


def gmina_view(request, **kwargs):
    kwargs['obj'] = Gmina.objects.get(pk=kwargs['id'])
    kwargs['wyniki'] = Kandydat.objects.filter(wyniki__obwod__gmina_id=kwargs['id'])

    return single_results_view(request, **kwargs)


def obwod_view(request, **kwargs):
    obwod = Obwod.objects.get(pk=kwargs['id'])

    if request.method == 'GET':
        kwargs['form'] = WynikiForm()
    else:
        form = WynikiForm(request.POST)
        try:
            with transaction.atomic():
                if form.is_valid():
                    data = form.cleaned_data
                    try_update_vote_count(obwod=obwod, kandydat=data['kandydat'], glosy=data['glosy'])
                    # Update data for display
                    obwod = Obwod.objects.get(pk=kwargs['id'])
        except IntegrityError:
            form.add_error('glosy', "Requested change is impossible due to bad resulting vote count.")
        kwargs['form'] = form
        kwargs['form_errors'] = form.errors

    kwargs['obj'] = obwod
    kwargs['wyniki'] = Kandydat.objects.filter(wyniki__obwod__id=obwod.id)
    kwargs['glosy_dostepne'] = obwod.glosy_niewazne + (obwod.wydane_karty - obwod.glosy_oddane)

    return single_results_view(request, template_name="obwod.html", **kwargs)


def try_update_vote_count(obwod, kandydat, glosy):
    wyniki_obwod = Wyniki.objects.select_for_update().filter(obwod=obwod)
    wyniki_kandydat = wyniki_obwod.filter(kandydat=kandydat)
    delta = glosy - wyniki_kandydat.get().glosy

    wyniki_kandydat.update(glosy=glosy)

    total_count = wyniki_obwod.aggregate(total=Sum('glosy'))['total']
    if total_count > obwod.wydane_karty:
        raise IntegrityError(_("There are more votes than it is possible."))
    else:
        obwod_queryset = Obwod.objects.filter(pk=obwod.id)
        glosy_oddane = obwod.glosy_oddane
        if delta > 0 and obwod.wydane_karty > obwod.glosy_oddane:
            nieoddane = min(obwod.wydane_karty - obwod.glosy_oddane, delta)
            glosy_oddane = obwod.glosy_oddane + nieoddane
            delta = delta - nieoddane  # Decrease available vote count

        obwod_queryset.update(glosy_oddane=glosy_oddane,
                              glosy_wazne=total_count,
                              glosy_niewazne=obwod.glosy_niewazne - delta)


def single_results_view(request, template_name="results_single.html", **kwargs):
    if 'obj' in kwargs:
        obj = kwargs['obj']
        kwargs['nav'] = build_navigation(obj)
        kwargs['nav_children'] = obj.get_nav_children_links()
    else:
        kwargs['nav'] = build_navigation()

    kwargs['result_set'] = calculate_results(kwargs['wyniki'])

    return render(request, template_name, kwargs)


def calculate_results(data):
    results = data.annotate(glosy=Sum('wyniki__glosy'))
    total_glosy = results.aggregate(total=Sum('glosy'))['total']
    results = results.annotate(percent=Sum('wyniki__glosy', output_field=FloatField()) * 100.0 / total_glosy)
    return results


def gmina_search_view(request, **kwargs):
    kwargs['nav'] = build_navigation()

    if request.GET.__len__() > 0:
        form = GminaSearchForm(request.GET)
    else:
        form = GminaSearchForm()

    results = ""
    if form.is_valid():
        nazwa = form.cleaned_data['nazwa']
        results = Gmina.objects.filter(nazwa__contains=nazwa).order_by('kod')

    return render(request, "gmina_search.html", {'form': form, 'results': results})


class GminaSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gmina
        fields = ('nazwa', 'link')

    link = serializers.SerializerMethodField('get_view_url')

    def get_view_url(self, obj):
        return obj.get_view_url()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class APIGminaListView(ListAPIView):
    serializer_class = GminaSearchSerializer

    def get_queryset(self):
        if 'contains' in self.kwargs:
            return Gmina.objects.filter(nazwa__contains=self.kwargs['contains']).order_by('kod')
        else:
            return Gmina.objects.all().order_by('kod')


class KandydatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kandydat
        fields = '__all__'

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        return Kandydat.objects.create(**validated_data)


class APIKandydatListView(ListAPIView):
    serializer_class = KandydatSerializer

    def get_queryset(self):
        if 'id' in self.kwargs:
            return Kandydat.objects.filter(id=self.kwargs['id'])
        else:
            return Kandydat.objects.all()


class APIWynikiView(APIView):
    allowed_results_types = ['wojewodztwo', 'okreg', 'gmina', 'obwod']

    def get(self, request, **kwargs):
        if 'type' not in kwargs:  # All results
            nazwa = 'Kraj'
            wyniki = Kandydat.objects.all()
        else:
            allowed_models = {'wojewodztwo': (Wojewodztwo, 'obwod__okreg__wojewodztwo_id'),
                              'okreg':       (Okreg,       'obwod__okreg_id'),
                              'gmina':       (Gmina,       'obwod__gmina_id'),
                              'obwod':       (Obwod,       'obwod__id')}

            if kwargs['type'] not in allowed_models:
                raise Http404()
            else:
                model_filters = allowed_models[kwargs['type']]

                obj = get_object_or_404(model_filters[0], pk=kwargs['id'])
                nazwa = str(obj)
                wyniki = Kandydat.objects.filter(**{'{0}__{1}'.format('wyniki', model_filters[1]): kwargs['id']})

        wyniki = calculate_results(wyniki).values('id', 'imie', 'nazwisko', 'glosy', 'percent')
        response_data = OrderedDict({'nazwa': nazwa, 'wyniki': wyniki})

        if 'type' in kwargs and kwargs['type'] == 'obwod':
            response_data['szczegoly'] = {
                'uprawnieni': obj.uprawnieni,
                'wydane_karty': obj.wydane_karty,
                'glosy_oddane': obj.glosy_oddane,
                'glosy_niewazne': obj.glosy_niewazne,
                'glosy_wazne': obj.glosy_wazne,
                'glosy_dostepne': obj.glosy_niewazne + (obj.wydane_karty - obj.glosy_oddane)
            }

        return Response(response_data)


class APIWynikiUpdateView(APIView):
    parser_classes = (JSONParser,)

    def post(self, request, **kwargs):
        obwod = get_object_or_404(Obwod, pk=kwargs['id'])

        try:
            with transaction.atomic():
                kandydat = get_object_or_404(Kandydat, pk=request.data['kandydat'])
                try_update_vote_count(obwod=obwod, kandydat=kandydat, glosy=request.data['glosy'])

                return Response(status=status.HTTP_200_OK)
        except IntegrityError:
            return Response({'error': 'Invalid vote count'}, status=status.HTTP_400_BAD_REQUEST)


class APIAuthGetToken(APIView):
    permission_classes = (AllowAny,)
    parser_classes = (JSONParser,)

    def post(self, request, **kwargs):
        user = authenticate(username=request.data['username'], password=request.data['password'])
        if user is not None:
            return Response({'token': Token.objects.get(user=user).key})
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class APIAuthUsername(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, **kwargs):
        return Response(request.user.username)
