from django.test import TestCase
from django.db import IntegrityError
from wybory.models import Wojewodztwo, Powiat, Okreg, Gmina, Obwod, Kandydat, Wyniki
from wybory.views import try_update_vote_count


class UpdateVoteCountTestCase(TestCase):
    def setUp(self):
        wojewodztwo = Wojewodztwo.objects.create(pk=1, nazwa="Testowe Wojewodztwo")
        powiat = Powiat.objects.create(pk=55, wojewodztwo=wojewodztwo, nazwa="Testowy Powiat")
        okreg = Okreg.objects.create(pk=3, wojewodztwo=wojewodztwo)
        gmina = Gmina.objects.create(pk=5, powiat=powiat, kod=12345, nazwa="Testowa Gmina")

        for i in range(1, 12):
            Kandydat.objects.create(pk=i, imie="Testowy", nazwisko="Kandydat {0}".format(i))

        obwod = Obwod.objects.create(pk=1, gmina_id=gmina.id, okreg_id=okreg.id, numer=123,
                                     adres="Testowy Obwod",
                                     typ='P', uprawnieni=2000, wydane_karty=1000, glosy_oddane=980, glosy_niewazne=15,
                                     glosy_wazne=965)

        for kandydat in Kandydat.objects.all():
            Wyniki.objects.create(kandydat_id=kandydat.id, obwod_id=obwod.id, glosy=0)

        Wyniki.objects.filter(kandydat_id=1, obwod_id=obwod.id).update(glosy=965)

    def test_add_consume_nieoddane_karty_first(self):
        """When increasing valid vote count, first try to consume available votes from (wydane_karty - glosy_oddane)"""
        obwod_before = Obwod.objects.get(pk=1)
        delta = 5
        try_update_vote_count(obwod=obwod_before, kandydat=Kandydat.objects.get(pk=1),
                              glosy=obwod_before.glosy_wazne + delta)

        obwod_after = Obwod.objects.get(pk=1)
        self.assertEqual(obwod_after.glosy_oddane, obwod_before.glosy_oddane + delta)

    def test_add_consume_nieoddane_karty_no_overflow(self):
        """When consuming nieoddane karty (wydane_karty - glosy_oddane) while increasing vote count, make sure we don't
           go over maximum allowed (wydane_karty)"""
        obwod_before = Obwod.objects.get(pk=1)
        (max_nieoddane, overflow) = (obwod_before.wydane_karty - obwod_before.glosy_oddane, 5)
        try_update_vote_count(obwod=obwod_before, kandydat=Kandydat.objects.get(pk=1),
                              glosy=obwod_before.glosy_wazne + max_nieoddane + overflow)

        obwod_after = Obwod.objects.get(pk=1)
        self.assertLessEqual(obwod_after.glosy_oddane, obwod_after.wydane_karty)

    def test_add_consume_nieoddane_karty_then_niewazne(self):
        """When consuming nieoddane karty while increasing vote count,
           make sure the overflow will be consumed from glosy_niewazne"""
        obwod_before = Obwod.objects.get(pk=1)
        (max_nieoddane, overflow) = (obwod_before.wydane_karty - obwod_before.glosy_oddane, 5)
        try_update_vote_count(obwod=obwod_before, kandydat=Kandydat.objects.get(pk=1),
                              glosy=obwod_before.glosy_wazne + max_nieoddane + overflow)

        obwod_after = Obwod.objects.get(pk=1)
        self.assertEquals(obwod_after.glosy_niewazne, obwod_before.glosy_niewazne - 5)

    def test_add_consume_all_available_votes(self):
        """When consuming nieoddane karty while increasing vote count,
                   make sure the overflow will be consumed from glosy_niewazne"""
        obwod_before = Obwod.objects.get(pk=1)
        (max_nieoddane, niewazne) = (obwod_before.wydane_karty - obwod_before.glosy_oddane, obwod_before.glosy_niewazne)
        try_update_vote_count(obwod=obwod_before, kandydat=Kandydat.objects.get(pk=1),
                              glosy=obwod_before.glosy_wazne + max_nieoddane + niewazne)

        obwod_after = Obwod.objects.get(pk=1)
        self.assertEquals(obwod_after.wydane_karty, obwod_after.glosy_oddane)
        self.assertEquals(obwod_after.glosy_niewazne, 0)

    def test_cannot_set_more_than_maximum_avaiable_votes(self):
        """Check if appropriate exception is thrown when trying to set more votes than it is available and if the data
           did not change"""
        obwod_before = Obwod.objects.get(pk=1)
        (max_nieoddane, niewazne) = (obwod_before.wydane_karty - obwod_before.glosy_oddane, obwod_before.glosy_niewazne)

        with self.assertRaises(IntegrityError):
            try_update_vote_count(obwod=obwod_before, kandydat=Kandydat.objects.get(pk=1),
                                  glosy=obwod_before.glosy_wazne + max_nieoddane + niewazne + 1)

        obwod_after = Obwod.objects.get(pk=1)
        self.assertEquals(obwod_before, obwod_after)