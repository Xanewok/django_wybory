import time
import asyncio

from django.contrib.auth.models import User
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from wybory.models import Gmina


# Selenium custom expected condition
class url_is(object):
    """ An expectation for checking that the title contains a case-sensitive
    substring. title is the fragment of title expected
    returns True when the title matches, False otherwise
    """
    def __init__(self, current_url):
        self.current_url = current_url

    def __call__(self, driver):
        return driver.current_url == self.current_url or driver.current_url == self.current_url + '/'


# Helper ContextManager class that disables driver.implicitly_wait for duration of context
class DisableImplicitWait:
    def __init__(self, driver, restored_implicit_wait=5):
        self.driver = driver
        self.restored_wait = restored_implicit_wait

    def __enter__(self):
        self.driver.implicitly_wait(0)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.implicitly_wait(self.restored_wait)


def login(driver, live_server_url, username, password, should_fail=False):
    login_url = '%s%s' % (live_server_url, '/login/')
    driver.get(login_url)
    username_input = driver.find_element_by_id("username")
    username_input.send_keys(username)
    password_input = driver.find_element_by_id("password")
    password_input.send_keys(password)
    driver.find_element_by_xpath('//input[@value="login"]').click()

    if should_fail:
        WebDriverWait(driver, 2).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, '#login-error'), "Error logging in"))
    else:
        assert (driver.find_element_by_id('login-error').text == "")
        WebDriverWait(driver, 2).until(url_is(live_server_url))
        assert (driver.current_url != login_url)


def assert_page_valid(driver, needs_login=False, needs_results=False):
    WebDriverWait(driver, 2).until(
        EC.presence_of_element_located((By.CLASS_NAME, "primary-header")))
    if needs_login:
        assert(driver.find_element_by_id("login-info"))
    if needs_results:
        WebDriverWait(driver, 2).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#single-results-table-body > tr:nth-child(1)')))

    # TODO: Check if all necessary elements are on current page,
    # TODO: such as navigation, login info, results table, search link etc.
    pass


class MySeleniumLoggedInTests(StaticLiveServerTestCase):
    fixtures = ['tests/basic.yaml']
    @classmethod
    def setUpClass(cls):
        super(MySeleniumLoggedInTests, cls).setUpClass()
        cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(MySeleniumLoggedInTests, cls).tearDownClass()

    def setUp(self):
        user = User.objects.create_user('admin', 'admin@example.com', 'admin')
        login(self.selenium, self.live_server_url, 'admin', 'admin')

    def test_update_propagates(self):
        RESULTS_WILECKI_VOTE_COUNT_CELL_SELECTOR = '#single-results-table-body > tr:nth-child(12) > td:nth-child(2)'
        AVAILABLE_VOTES_SELECTOR = '#results-details > ul:nth-child(2) > li:nth-child(6)'
        VOTE_COUNT_INCREASE = 2

        driver = self.selenium
        # For each item in navigation remember old vote count to compare against
        old_votes = list()
        # Bypass evil Google Geochart map (can't reasonable select
        # navigable object, even with Selenium IDE, clicks don't work)
        for route in ["/", "/wojewodztwo/20853"]:
            driver.get(self.live_server_url + route)
            WebDriverWait(driver, 2).until(EC.visibility_of_element_located(
                (By.CSS_SELECTOR, RESULTS_WILECKI_VOTE_COUNT_CELL_SELECTOR)))
            old_votes.append(int(driver.find_element_by_css_selector(RESULTS_WILECKI_VOTE_COUNT_CELL_SELECTOR).text))

        # After Geochart map navigate regularly via site navigation
        for link_text in [u"Okręg nr 32", "Zabrodzie", u"Publiczna Szkoła Podstawowa Zabrodzie"]:
            driver.find_element_by_link_text(link_text).click()
            old_votes.append(int(driver.find_element_by_css_selector(RESULTS_WILECKI_VOTE_COUNT_CELL_SELECTOR).text))

        available_votes = driver.find_element_by_css_selector(AVAILABLE_VOTES_SELECTOR)
        available_votes = int(available_votes.text.split("Głosy dostępne: ", 1)[1])
        assert(available_votes >= VOTE_COUNT_INCREASE)

        current_votes = old_votes[-1]
        new_votes = current_votes + VOTE_COUNT_INCREASE

        # Do update via form on website
        Select(driver.find_element_by_id("candidate-id")).select_by_visible_text("Tadeusz Wilecki")
        driver.find_element_by_id("vote-count").clear()
        driver.find_element_by_id("vote-count").send_keys(new_votes)
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()

        # Check if available votes has updated correctly
        WebDriverWait(driver, 1).until(EC.text_to_be_present_in_element(
            (By.CSS_SELECTOR, AVAILABLE_VOTES_SELECTOR),
            str(available_votes - VOTE_COUNT_INCREASE)))

        # Check if value in results table has updated correctly
        def assert_results_vote_count():
            WebDriverWait(driver, 1).until(EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, RESULTS_WILECKI_VOTE_COUNT_CELL_SELECTOR),
                str(old_votes.pop() + VOTE_COUNT_INCREASE)))

        assert_results_vote_count()
        # Check updated vote count in all parent views
        for link_text in ["Zabrodzie", u"Okręg nr 32", "Mazowieckie", "Kraj"]:
            driver.find_element_by_link_text(link_text).click()
            assert_results_vote_count()


class MySeleniumTests(StaticLiveServerTestCase):
    fixtures = ['tests/basic.yaml']
    @classmethod
    def setUpClass(cls):
        super(MySeleniumTests, cls).setUpClass()
        cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(MySeleniumTests, cls).tearDownClass()

    def setUp(self):
        user = User.objects.create_user('admin', 'admin@example.com', 'admin')

    def test_login_logout_valid_user(self):
        login(self.selenium, self.live_server_url, 'admin', 'admin', should_fail=False)
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_link_text('Wyloguj').click()
        WebDriverWait(self.selenium, 2).until(EC.presence_of_element_located((By.CLASS_NAME, "logged-out-notice")))
        assert("poprawne wylogowanie" in self.selenium.find_element_by_class_name("logged-out-notice").text)

    def test_login_invalid_user(self):
        login(self.selenium, self.live_server_url, 'invalid', 'username', should_fail=True)

    def test_search(self):
        SEARCH_VALUE = "zg"
        driver = self.selenium
        driver.get(self.live_server_url + "/gmina/search/")
        driver.find_element_by_id("gmina-search-name").clear()
        driver.find_element_by_id("gmina-search-name").send_keys(SEARCH_VALUE)
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        WebDriverWait(self.selenium, 2).until(EC.presence_of_element_located((By.CSS_SELECTOR, "#gmina-search-results > ul")))

        web_items = driver.find_element_by_css_selector("#gmina-search-results > ul").find_elements_by_tag_name('li')

        items = list(map(lambda x: x.text, web_items))
        db_items = list(map(lambda x: x['nazwa'], Gmina.objects.filter(nazwa__contains=SEARCH_VALUE).values('nazwa')))
        assert(items.sort() == db_items.sort())

        if len(web_items) > 0:
            ActionChains(driver)\
                .move_to_element(driver.find_element_by_partial_link_text(SEARCH_VALUE)).click().perform()
            assert_page_valid(driver, needs_login=True, needs_results=True)

    def test_basic_site_navigation(self):
        HOMEPAGE_NAV_SELECTOR = "body > header > div > a"

        driver = self.selenium
        driver.get(self.live_server_url)
        # Check validity of sites accessible from homepage
        for feature in [("Zaloguj", False, False), ("Wyszukaj gminę", True, False)]:
            driver.find_element_by_link_text(feature[0]).click()
            assert_page_valid(driver, needs_login=feature[1])
            driver.find_element_by_css_selector(HOMEPAGE_NAV_SELECTOR).click()
            assert_page_valid(driver, needs_login=feature[1], needs_results=feature[2])

        # Bypass Google Geochart
        driver.get(self.live_server_url + "/wojewodztwo/20853")
        # After Geochart map navigate regularly via site navigation
        for link_text in [u"Okręg nr 32", "Zabrodzie", u"Publiczna Szkoła Podstawowa Zabrodzie",
                          "Zabrodzie", u"Okręg nr 32", "Mazowieckie", "Kraj"]:
            driver.find_element_by_link_text(link_text).click()
            assert_page_valid(driver, needs_login=True, needs_results=True)
            # We shouldn't be able to edit votes as user that is not logged in
            with DisableImplicitWait(driver):
                if '/obwod/' not in driver.current_url:
                    assert (len(driver.find_elements_by_class_name('vote-edit-container')) == 0)
                else:
                    WebDriverWait(driver, 1).until(
                        EC.invisibility_of_element_located((By.CLASS_NAME, 'vote-edit-container')))

    def test_logged_user_can_edit_votes(self):
        driver = self.selenium
        login(driver, self.live_server_url, 'admin', 'admin', should_fail=False)
        # Bypass Google Geochart
        driver.get(self.live_server_url + "/wojewodztwo/20853")
        # After Geochart map navigate regularly via site navigation
        for link_text in [u"Okręg nr 32", "Zabrodzie", u"Publiczna Szkoła Podstawowa Zabrodzie"]:
            driver.find_element_by_link_text(link_text).click()
        # Verify thet vote edit container is visible
        WebDriverWait(driver, 1).until(EC.visibility_of_element_located((By.CLASS_NAME, 'vote-edit-container')))


class MySeleniumMultipleClients(StaticLiveServerTestCase):
    fixtures = ['tests/basic.yaml']
    @classmethod
    def setUpClass(cls):
        super(MySeleniumMultipleClients, cls).setUpClass()
        cls.drivers = [webdriver.Chrome(), webdriver.Chrome()]

    @classmethod
    def tearDownClass(cls):
        for driver in cls.drivers:
            driver.quit()
        super(MySeleniumMultipleClients, cls).tearDownClass()

    def setUp(self):
        for (i, driver) in enumerate(self.drivers):
            username = 'admin' + str(i)
            User.objects.create_user(username, username + '@example.com', 'admin')

    def test_disallow_simultaneous_vote_edit(self):
        # Get available vote count
        AVAILABLE_VOTES = 21  # Use data from checked test case

        # Use async to avoid blocking calls to drivers, which result in dead-lock
        async def prepare_until_obwod(i, driver):
            login(driver, self.live_server_url, 'admin' + str(i), 'admin')
            # Bypass Google Geochart
            driver.get(self.live_server_url + "/wojewodztwo/20853")
            # After Geochart map navigate regularly via site navigation
            for link_text in [u"Okręg nr 32", "Zabrodzie", u"Publiczna Szkoła Podstawowa Zabrodzie"]:
                driver.find_element_by_link_text(link_text).click()

        async def try_update_vote_count(driver, candidate_id):
            results_row_selector_format = '#single-results-table-body > tr:nth-child({0}) > td:nth-child({1})'
            candidate_name_selector = results_row_selector_format.format(candidate_id + 1, 1)
            vote_count_selector = results_row_selector_format.format(candidate_id + 1, 2)

            candidate_name = driver.find_element_by_css_selector(candidate_name_selector).text
            current_vote_count = int(driver.find_element_by_css_selector(vote_count_selector).text)

            Select(driver.find_element_by_id("candidate-id")).select_by_visible_text(candidate_name)
            driver.find_element_by_id("vote-count").clear()
            driver.find_element_by_id("vote-count").send_keys(str(AVAILABLE_VOTES + current_vote_count))
            driver.find_element_by_css_selector("input[type=\"submit\"]").click()

        loop = asyncio.get_event_loop()

        futures = [prepare_until_obwod(i, driver) for (i, driver) in enumerate(self.drivers)]
        loop.run_until_complete(asyncio.wait(futures))
        futures = [try_update_vote_count(drv, cand) for (drv, cand) in [(self.drivers[0], 2), (self.drivers[1], 5)]]
        loop.run_until_complete(asyncio.wait(futures))

        # Check if one of those drivers encountered a problem
        errors = list(map(lambda drv: drv.find_element_by_id('vote-edit-error'), self.drivers))
        assert(errors[0].is_displayed() != errors[1].is_displayed())
        # print("Error 0 is displayed: " + str(errors[0].is_displayed()))
        # print("Error 1 is displayed: " + str(errors[1].is_displayed()))
