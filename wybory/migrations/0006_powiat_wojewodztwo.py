# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-22 19:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wybory', '0005_auto_20170421_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='powiat',
            name='wojewodztwo',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='wybory.Wojewodztwo'),
            preserve_default=False,
        ),
    ]
