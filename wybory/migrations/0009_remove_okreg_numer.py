# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-22 23:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wybory', '0008_auto_20170422_2136'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='okreg',
            name='numer',
        ),
    ]
