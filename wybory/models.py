from collections import OrderedDict
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


# Make sure every User that's created will have his REST API Token generated
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def validate_non_negative(value):
    if value < 0:
        raise ValidationError(
            _('%(value)s is lesser than 0'),
            params={'value': value},
        )


# Create your models here.
class Kandydat(models.Model):
    class Meta:
        verbose_name = "kandydat"
        verbose_name_plural = "kandydaci"

    imie = models.CharField(max_length=42)
    nazwisko = models.CharField(max_length=42)

    def __str__(self):
        return "{0} {1}".format(self.imie, self.nazwisko)


class Wojewodztwo(models.Model):
    class Meta:
        verbose_name = "województwo"
        verbose_name_plural = "województwa"

    nazwa = models.CharField(max_length=42)

    def __str__(self):
        return "{0}".format(self.nazwa)

    def get_view_url(self):
        return "/wojewodztwo/{0}".format(self.id)

    def get_nav_parent(self):
        return None

    def get_nav_parent_link(self):
        return {"Kraj": "/"}

    def get_nav_children_links(self):
        children = OrderedDict()
        okregi = Okreg.objects.filter(wojewodztwo=self)
        for okreg in okregi:
            children[str(okreg)] = okreg.get_view_url()
        return children


class Okreg(models.Model):
    class Meta:
        verbose_name = "okręg"
        verbose_name_plural = "okręgi"

    wojewodztwo = models.ForeignKey(Wojewodztwo)

    def __str__(self):
        return "Okręg nr {0}".format(self.id)

    def get_view_url(self):
        return "/okreg/{0}".format(self.id)

    def get_nav_parent(self):
        return self.wojewodztwo

    def get_nav_parent_link(self):
        parent = self.get_nav_parent()
        return {str(parent): parent}

    def get_nav_children_links(self):
        children = OrderedDict()
        # Haxx (caused by ambiguious tree structure)
        gminy = Gmina.objects.filter(obwod__okreg=self).order_by('nazwa')
        for gmina in gminy:
            children[str(gmina)] = gmina.get_view_url()
        return children


class Powiat(models.Model):
    class Meta:
        verbose_name = "powiat"
        verbose_name_plural = "powiaty"

    wojewodztwo = models.ForeignKey(Wojewodztwo)
    nazwa = models.CharField(max_length=30)

    def __str__(self):
        return "{0}".format(self.nazwa)


class Gmina(models.Model):
    class Meta:
        verbose_name = "gmina"
        verbose_name_plural = "gminy"

    powiat = models.ForeignKey(Powiat)
    kod = models.IntegerField()
    nazwa = models.CharField(max_length=42)

    def __str__(self):
        return "{0}".format(self.nazwa)

    def get_view_url(self):
        return "/gmina/{0}".format(self.id)

    def get_nav_parent(self):
        # Haxx (caused by ambiguious tree structure)
        obwod = Obwod.objects.filter(gmina=self.id).first()
        return obwod.okreg

    def get_nav_parent_link(self):
        parent = self.get_nav_parent()
        return {str(parent): parent}

    def get_nav_children_links(self):
        children = OrderedDict()
        obwody = Obwod.objects.filter(gmina=self)
        for obwod in obwody:
            children[str(obwod)] = obwod.get_view_url()
        return children


class Obwod(models.Model):
    class Meta:
        verbose_name = "obwód"
        verbose_name_plural = "obwody"

    TYP_OBWODU = (
        ('P', 'P'),
        ('L', 'L'),
    )
    gmina = models.ForeignKey(Gmina)
    okreg = models.ForeignKey(Okreg)
    numer = models.IntegerField()
    adres = models.CharField(max_length=255)
    typ = models.CharField(max_length=2, choices=TYP_OBWODU)

    uprawnieni = models.IntegerField(validators=[validate_non_negative])
    wydane_karty = models.IntegerField(validators=[validate_non_negative])
    glosy_oddane = models.IntegerField(validators=[validate_non_negative])
    glosy_niewazne = models.IntegerField(validators=[validate_non_negative])
    glosy_wazne = models.IntegerField(validators=[validate_non_negative])

    wyniki_kandydatow = models.ManyToManyField(
        Kandydat,
        through='Wyniki',
        through_fields=('obwod', 'kandydat'),
    )

    def clean(self):
        if self.wydane_karty > self.uprawnieni:
            raise ValidationError(_('Wydanych kart jest więcej niż uprawnionych'))
        if self.glosy_oddane > self.wydane_karty:
            raise ValidationError(_('Głosów oddanych jest więcej niż wydanych kart'))
        if self.glosy_wazne + self.glosy_niewazne > self.glosy_oddane:
            raise ValidationError(_('Głosów ważnych i nieważnych jest wiecej niż oddanych'))

    def __str__(self):
        return "{0}".format(self.adres)

    def get_view_url(self):
        return "/obwod/{0}".format(self.id)

    def get_nav_parent(self):
        return self.gmina

    def get_nav_parent_link(self):
        parent = self.get_nav_parent()
        return {str(parent): parent}

    def get_nav_children_links(self):
        return {}


class Wyniki(models.Model):
    class Meta:
        verbose_name = "wyniki"
        verbose_name_plural = "wyniki"

    obwod = models.ForeignKey(Obwod, on_delete=models.CASCADE)
    kandydat = models.ForeignKey(Kandydat, on_delete=models.CASCADE)

    glosy = models.IntegerField(validators=[validate_non_negative])


def build_navigation(obj=None):
    nav = OrderedDict()

    while obj:
        nav[str(obj)] = obj.get_view_url()
        obj = obj.get_nav_parent()
    nav["Kraj"] = "/"

    return OrderedDict(reversed(list(nav.items())))
