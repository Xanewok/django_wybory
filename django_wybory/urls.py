"""django_wybory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from wybory.views import *
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url('^', include('django.contrib.auth.urls')),  # Default Django login auth
    url(r'^login[/]*$', TemplateView.as_view(template_name="registration/login.html"), name='login'),
    url(r'^logout[/]*$', TemplateView.as_view(template_name="registration/logged_out.html"), name='logout'),


    url(r'^$', kraj_view),
    url(r'^wojewodztwo/(?P<id>[0-9]+)$', wojewodztwo_view),
    url(r'^okreg/(?P<id>[0-9]+)$', okreg_view),
    url(r'^gmina/(?P<id>[0-9]+)$', gmina_view),
    url(r'^obwod/(?P<id>[0-9]+)$', obwod_view),
    url(r'^gmina/search[/]*$', gmina_search_view),

    url(r'^api/gmina/search/(?P<contains>(.*))$', APIGminaListView.as_view()),

    url(r'^api/kandydat[/]*$', APIKandydatListView.as_view()),
    url(r'^api/kandydat/(?P<id>[0-9]+)$', APIKandydatListView.as_view()),
    url(r'^api/wyniki[/]*$', APIWynikiView.as_view()),
    url(r'^api/wyniki/(?P<type>[a-z]+)/(?P<id>[0-9]+)[/]*$', APIWynikiView.as_view()),
    url(r'^api/wyniki/obwod/(?P<id>[0-9]+)/update[/]*$', APIWynikiUpdateView.as_view()),
    url(r'^api/auth/get_token', APIAuthGetToken.as_view()),
    url(r'^api/auth/username', APIAuthUsername.as_view()),
    # url(r'^api-auth/', include('rest_framework.urls')),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
