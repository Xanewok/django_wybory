/* jshint -W097 */ 'use strict';

if (typeof(Storage) !== "undefined") {
  localStorage.removeItem("api-auth-token");
  localStorage.removeItem("username");

  document.getElementById('login-info').remove();
}
