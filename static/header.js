/* jshint -W097 */ 'use strict';

if (typeof(Storage) !== "undefined") {
  var username = localStorage.getItem('username');

  if (username !== null) {
    var loginInfo = document.createElement('div');
    document.getElementById('login-info').replaceWith(loginInfo);
    loginInfo.id = "login-info";
    loginInfo.innerHTML = "Zalogowany jako " + username + " ";
    var logout = document.createElement('a');
    logout.href = "/logout";
    logout.innerText = "Wyloguj";
    loginInfo.appendChild(logout);
  }
}
