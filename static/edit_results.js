/* jshint -W097 */ 'use strict';

function updateCandidateChoices() {
  var req = new XMLHttpRequest();
  req.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      var json = JSON.parse(req.responseText);

      var select = document.getElementById('candidate-id');
      json.forEach(function(cand) {
        var option = document.createElement('option');
        option.value = cand.id;
        option.innerText = cand.imie + " " + cand.nazwisko;

        select.appendChild(option);
      });

    }
  };
  req.open("GET", "/api/kandydat/", true);
  req.send();
}

function hideVoteCountError() {
  var errorElem = document.getElementById('vote-edit-error');
  errorElem.style.cssText = "visibility: hidden";
}

function displayVoteCountError(msg) {
  var errorElem = document.getElementById('vote-edit-error');
  errorElem.style.cssText = "visibility: visible";
  errorElem.innerHTML = msg;
}

function updateVoteCount(event) {
  event.preventDefault();


  var kandydat = Number(document.getElementById('candidate-id').value);
  var glosy = Number(document.getElementById('vote-count').value);
  if (isNaN(kandydat) || isNaN(glosy) || (glosy % 1) !== 0 || glosy < 0) {
    if (!kandydat) { displayVoteCountError("Kandydat jest niepoprawny"); }
    else { displayVoteCountError("Podana liczba głosów jest niepoprawna"); }

    return;
  }

  if (typeof(Storage) === "undefined" || !localStorage.getItem('api-auth-token')) {
    displayVoteCountError("Tylko zalogowani użytkownicy mogą zmieniać głosy");
    return;
  }

  var obwodId = window.location.pathname.split('/')[2];

  var req = new XMLHttpRequest();
  req.open("POST", '/api/wyniki/obwod/' + obwodId + '/update', true);
  req.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      hideVoteCountError();

      var pathname = window.location.pathname.split('/');
      var endpoint = "/api/wyniki/" + (window.location.pathname === '/' ? '' : pathname[1] + '/' + pathname[2]);

      // Refresh results if everything went alright
      processDynamicNode(SINGLE_RESULTS_TBODY_ID, constructResultsNode, endpoint, 0);
    } else if (this.readyState === 4 && this.status !== 200) {
      var json = null;
      try { json = JSON.parse(req.responseText); } catch (e) { json = null; }

      displayVoteCountError((json !== null && json.error !== null) ? json.error : "Request Error: " + this.status);
    }
  };

  req.setRequestHeader('Content-Type', 'application/json');
  req.setRequestHeader('Authorization', 'Token ' + localStorage.getItem('api-auth-token'));
  req.send(JSON.stringify({
    kandydat: kandydat,
    glosy: glosy
  }));
}

hideVoteCountError();
updateCandidateChoices();

if (typeof(Storage) === "undefined" || !localStorage.getItem('username')) {
  document.getElementById('vote-edit').style.cssText = "visibility: hidden";
}