/* jshint -W097 */ 'use strict';

/**
 * Constructs and returns a created DOM subtree containing result section.
 * @param {string} nodeId ID that will be given to created DOM node.
 * @param {string} jsonData JSON data string representing results.
 * @return {HTMLElement} Results node element.
 */
function constructResultsNode(nodeId, jsonData) {
  var voteCounts = jsonData.wyniki.map(function(wynik) { return wynik.glosy; }).sort(function(a, b) { return b - a; });

  var tbody = document.createElement('tbody');
  tbody.id = nodeId;

  jsonData.wyniki.forEach(function(wynik) {
    var tr = document.createElement('tr');

    var name = document.createElement('td');
    name.innerHTML = wynik.imie + ' ' + wynik.nazwisko;
    tr.appendChild(name);

    var voteCount = document.createElement('td');
    voteCount.innerHTML = wynik.glosy;
    tr.appendChild(voteCount);

    var votePercent = document.createElement('td');
    votePercent.innerHTML = wynik.percent.toFixed(1) + '%';
    tr.appendChild(votePercent);

    var voteBar = document.createElement('td');
    var bar = document.createElement('div');

    bar.className = 'vote-perc-bar';
    if (wynik.glosy === voteCounts[0] || wynik.glosy === voteCounts[1]) {
        bar.classList.add(wynik.glosy === voteCounts[0] ? 'vote-perc-bar-winner' : 'vote-perc-bar-2nd-best');
    }

    bar.style = 'width: ' + wynik.percent.toFixed(1) + '%';
    voteBar.appendChild(bar);
    tr.appendChild(voteBar);

    tbody.appendChild(tr);
  });

  return tbody;
}

function constructDetailsNode(json) {
  var node = document.createElement('div');
  node.id = 'results-details';
  var p = document.createElement('p');
  node.appendChild(p);
  p.innerText = "Szczegóły:";
  var ul = document.createElement('ul');
  node.appendChild(ul);

  var keys = [['uprawnieni', "Uprawnieni"], ['wydane_karty', "Wydane karty"], ['glosy_oddane', 'Głosy oddane'],
  ['glosy_wazne', 'Głosy ważne'], ['glosy_niewazne', 'Głosy nieważne'], ['glosy_dostepne', 'Głosy dostępne']];
  keys.forEach(function(pair) {
    var li = document.createElement('li');
    li.innerText = pair[1] + ": " + json[pair[0]];
    ul.appendChild(li);
  });

  return node;
}

/**
 * Tries to update a node with given id from localStorage and updated dynamically via AJAX GET request.
 * @param {string} nodeId ID of the node that will be updated from cache and AJAX GET request.
 * @param {function} nodeConstructor A function that given JSON data object creates and returns a DOM HTML node.
 * @param {function} endpoint AJAX endpoint which will be queried for data.
 * @param {number} delay Milliseconds until results will be queried and data updated.
 */
function processDynamicNode(nodeId, nodeConstructor, endpoint) {
  if (document.getElementById(nodeId) === null) {
    return;
  }

  var storageItemKey = nodeId + "+" + window.location.pathname;

  // Try to retrieve cached results
  if (typeof(Storage) !== "undefined") {
    var cachedRequest = localStorage.getItem(storageItemKey);
    if (cachedRequest) {
      var json = JSON.parse(cachedRequest);
      document.getElementById(nodeId).replaceWith(nodeConstructor(nodeId, json));

      var details = document.getElementById('results-details');
      if (details !== null && json.szczegoly !== null) {
        var newDetails = constructDetailsNode(json.szczegoly);
        details.replaceWith(newDetails);
      }
    }
  }

  var req = new XMLHttpRequest();
  req.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      var json = JSON.parse(req.responseText);
      document.getElementById(nodeId).replaceWith(nodeConstructor(nodeId, json));

      var details = document.getElementById('results-details');
      if (details !== null && json.szczegoly !== null) {
        var newDetails = constructDetailsNode(json.szczegoly);
        details.replaceWith(newDetails);
      }

      if (typeof(Storage) !== "undefined") {
          localStorage.setItem(storageItemKey, req.responseText);
      }
    }
  };

  req.open("GET", endpoint, true);
  req.send();
}

var SINGLE_RESULTS_TBODY_ID = 'single-results-table-body';

var pathname = window.location.pathname.split('/');
var results_endpoint = "/api/wyniki/" + (window.location.pathname === '/' ? '' : pathname[1] + '/' + pathname[2]);
// Declare dynamic node (loaded from localStorage cache and dynamically updated with AJAX)

processDynamicNode(SINGLE_RESULTS_TBODY_ID, constructResultsNode, results_endpoint);
