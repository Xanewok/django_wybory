/* jshint -W097 */ 'use strict';

function getQueryVariable(variable)
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] === variable) {
      return pair[1];
    }
  }
  return '';
}

function submitLoginCredentials(event) {
  event.preventDefault();
  var username = document.getElementById('username').value;

  var req = new XMLHttpRequest();
  req.open("POST", '/api/auth/get_token', true);
  req.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
        console.log(req.responseText);

      if (typeof(Storage) !== "undefined") {
        var json = JSON.parse(req.responseText);
        localStorage.setItem("api-auth-token", json.token);
        localStorage.setItem("username", username);

        window.location.href = window.location.origin + getQueryVariable('next');
      } else {
        document.getElementById('login-error').innerHTML = "Cannot store authentication data";
      }


    } else if (this.readyState === 4 && this.status !== 200) {
      document.getElementById('login-error').innerHTML = "Error logging in";
    }
  };

  req.setRequestHeader('Content-Type', 'application/json');
  req.send(JSON.stringify({
    username: username,
    password: document.getElementById('password').value
  }));
}

document.getElementById('login-info').remove();
