/* jshint -W097 */ 'use strict';

function updateSearchResults(event) {
  event.preventDefault();

  var req = new XMLHttpRequest();
  req.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      // Typical action to be performed when the document is ready:
      var json = JSON.parse(req.responseText);

      var elem = document.createElement('div');
      elem.id = "gmina-search-results";
      var header = document.createElement('h4');
      header.innerText = "Wyniki:";
      elem.appendChild(header);
      var ul = document.createElement('ul');
      elem.appendChild(ul);

      json.forEach(function(elem) {
        var li = document.createElement('li');
        var a = document.createElement('a');
        a.href = elem.link;
        a.innerText = elem.nazwa;
        li.appendChild(a);
        ul.appendChild(li);
      });

      document.getElementById("gmina-search-results").replaceWith(elem);
    }
  };
  req.open("GET", "/api/gmina/search/" + document.getElementById("gmina-search-name").value, true);
  req.send();
}
